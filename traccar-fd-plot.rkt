#lang racket/base
(require plot racket/string)

(plot-new-window? #t)

(define (port->points port)
  (for/list ([ln (in-lines port)]
             #:when (= 2 (length (string-split ln "," #:trim? #t))))
    (list->vector (map string->number (string-split ln "," #:trim? #t)))))

(define (file->points name)
  (port->points (open-input-file name)))

(define (file->plot #:in in #:out out [from 1500313818] [to 1512293365])
  (parameterize ([plot-x-ticks (date-ticks)])
    (plot (points (file->points in) #:size 1)
          #:x-min from
          #:x-max to
          #:y-min 0 #:y-max 35000
          #:width 3600
          #:x-label #f
          #:y-label "# fd abertos"
          #:title "DoS ataques sobre o Traccar"
          #:out-file out)))

;; List-of Points Timestamp Timestamp -> List-of Points
(define (filter-points ls-of-points ts-beg ts-end)
  "Produces points whose dates fall between BEG and END."
  (filter (lambda (vec)
            (let ([ts (vector-ref vec 0)])
              (<= ts-beg ts ts-end))) ls-of-points))

(define (doit)
  (let ([special-period-beg 1507420800]
        [special-period-end 1507593600])
    (file->plot #:in "fd-logger.csv" #:out "entire.pdf")
    (file->plot #:in "fd-logger.csv" #:out "special.pdf"
                special-period-beg special-period-end)))

(module+ main 
  (doit)
  (displayln "Graphs generated."))
